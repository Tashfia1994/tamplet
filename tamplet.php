<?php
$fonts= "vardana";
$bgcolor="#444";
$fontcolor="#FC8C41";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHP Syntex </title>
    <style>
        body{font-family: <?php echo $fonts;  ?>}
        .phpcoding{width:900px; margin: 0 auto;background:#ddd;}
        .headeroption, .footeroption{background:<?php echo $bgcolor; ?>;color:<?php echo $fontcolor; ?>;text-align: center;padding: 20px;}
        .headeroption h2, .footeroption h2{margin:0}
        .maincontent{min-height:400px;padding: 20px;}
    </style>
</head>

<body>

<div class="phpcoding">
    <section class="headeroption">
        <h2>PHP Fundamental</h2>
    </section>

    <section class="maincontent">



        <?php

        $a=30;
        $b=40;
        $c=$a+$b;
        echo "Total value =".$c ;
        ?>


    </section>
    <section class="footeroption">
        <h2>TAMPLATES</h2>
    </section>

</div>


</body>
</html>